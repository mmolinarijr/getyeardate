class CurrentYear {
    constructor() {
        this.name = 'Get Current year'
    }

    getYear() {
        return new Date().getFullYear();
    }
}

export default CurrentYear;