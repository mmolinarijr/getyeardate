# Get the current year date

This package aims to provide an easy way to get the current year date.

How to use it:

    1 - Import the module;
    2 - Instantiate new Object;
    3 - Call the method: getYear();
