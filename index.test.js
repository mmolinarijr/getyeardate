import CurrentYear from "./index";

test('Validate if class gets the current Year', () => {
    const getYear = new CurrentYear;
    expect(getYear.getYear()).toBe(new Date().getFullYear())
})